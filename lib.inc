section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
    mov     rdx, rax         ; string length in bytes
    mov     rsi, rdi         ; string address    
    mov     rax, 1           ; 'write' syscall number
    mov     rdi, 1           ; stdout descriptor
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi 
    mov rdx, 1      
    mov rsi, rsp 
    pop rdi 
    mov rax, 1      
    mov rdi, 1 
    syscall         
    ret 

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n'
    syscall
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rsi, 10
    push word 0
    mov rdi, rsp
    sub rsp, 24
    .loop:
        xor rdx, rdx
        div rsi
        or dl, '0'
        dec rdi
        mov [rdi], dl
        test rax, rax 
        jne .loop
        call print_string
        add rsp, 24 + 2 
        ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .print_number ; >= 0

    neg rdi
    push rdi

    mov rdi, '-' ; print -
    call print_char

    pop rdi
    .print_number:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
    .loop:
        mov r8b, [rdi + rcx]
		mov r9b, [rsi + rcx]
		cmp r8b, r9b
 		jne .end
		cmp byte[rdi + rcx], 0
		je .success
        inc rcx
		jmp .loop
    .success:
        inc rax
        jmp .end
    .end:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax 
    xor rdi, rdi ; stdin
    mov rdx, 1
    push rax 
    mov rsi, rsp 
    syscall
    pop rax 
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r10	
	xor	r10, r10
	mov	r8, rdi
	mov	r9, rsi
    .loop:
        call read_char
        cmp	rax, ' '
        je .check_word
        cmp	rax, 0x9
        je .check_word
        cmp	rax, '\n'
        je .check_word
        cmp	rax, 0
        jz .write_end
        cmp r10, r9
        jae	.end
        mov	byte [r8 + r10], al	
        inc	r10
        jmp	.loop
    .check_word:
        test r10, r10
        jz .loop
    .write_end:
        mov	byte [r8 + r10], 0
    .break:
        mov	rax, r8
        mov	rdx, r10
        pop	r10
        ret
    .end:
        mov	rax, 0
        pop	r10
        ret
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    .loop:
        mov r8b, [rdi + rdx]
        cmp r8b, '0'
        jb .end
        cmp r8b, '9'
        ja .end
        inc rdx
        sub r8b, '0'
        imul rax, 10
        add rax, r8
        jmp .loop
    .end:
	    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    push rdi
    call parse_uint
    pop rdi
    inc rdx
    neg rax
    ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    call string_length
    cmp rdx, rax
    js .error
    xor rdx, rdx
    .loop:
        mov rcx, [rdi + rdx]
        mov [rsi + rdx], rcx
        inc rdx
        cmp byte[rdi + rdx], 0
        jne .loop
    xor rcx, rcx
    mov [rsi + rdx], rcx
    ret
    .error:
        xor rax, rax
        ret

